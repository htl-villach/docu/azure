# Azure WebApps

## Deployment MERN Stack in Azure

Hinweis: Unter Windows muss der Backslash am Ende der Zeile durch ein Backtick ersetzt werden.

```shell

# replace the following placeholders (including the angle brackets)
# <RG-Name> .... e.g. RG-TasksApp
# <AppName> ... e.g. tasks-app (needs to be globally unique and always lowercase) 
# <Location> ... e.g. westeurope

# create ressource group for deployment
az group create -n <RG-Name> -l <Location>

# Create MongoDB
az cosmosdb create --name <AppName>-db \
  --resource-group <RG-Name> \
  --locations regionName=<Location> \
  --kind mongodb \
  --enable-free-tier true

# Create a webapp plan
az appservice plan create --name <AppName>-plan \
  --resource-group <RG-Name> \
  --location <Location> \
  --sku FREE \
  --is-linux

# List runtimes for overview (just for overview)
az webapp list-runtimes --os linux

# Create webapp
az webapp create --name <AppName> \
  --plan <AppName>-plan \
  --resource-group <RG-Name> \
  --runtime NODE:18-lts

# List connection strings
az cosmosdb keys list --name <AppName>-db \
  --resource-group <RG-Name> \
  --type connection-strings

# Set environment variables
az webapp config appsettings set --resource-group <RG-Name> \
  --name <AppName> \
  --settings MONGODB_CONNECTION_STRING='mongodb://...'

# Set environment variables
az webapp config appsettings set --resource-group <RG-Name> \
  --name <AppName> \
  --settings SCM_DO_BUILD_DURING_DEPLOYMENT=true

# Now.... generate zip file without node_modules
# Assuming file name is your-zip-file.zip

# Deploy webapp as zip file
az webapp deploy --resource-group <RG-Name> \
  --name <AppName> \
  --type zip \
  --src-path your-zip-file.zip

# Watch the logs of the hosted webapp
az webapp log tail --resource-group <RG-Name> \
  --name <AppName>
```
