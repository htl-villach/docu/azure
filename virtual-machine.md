# Azure Virtual Machines

## Generelle Anmerkungen

* User und Password sollten selbstverständlich besser gewählt werden.
* Die VM wird standardmäßig mit einer Network Security Group (NSG) erzeugt. Dies ist eine Art Firewall von Azure und erlaubt es bestimmten Datenverkehr zu erlauben bzw. zu verbieten. In diesem Beispiel wird diese NSG explizit deaktiviert, da dies bei Netzwerkübungen oft eine zusätzliche Fehlerquelle darstellt.
* Aus Kostengründen sollte als Storage eine reguläre HDD anstelle einer
SSD verwendet werden.  Dies ist für die meisten
schulischen Anwendungen ausreichend und hilft beim Sparen, da die
SSD um den Faktor 3-4 teurer ist.
* Es empfiehlt sich, die Auto-Shutdown Funktion für die VM zu nutzen. Vergisst man in der
Stunde einmal darauf, die VM zu deallokieren, so geschieht dies automatisch und es
fallen dann keine unabsichtlich verursachten Kosten an.

## Erstellung mittels Azure Portal

### Erstellung einer Ressource Group

![](/images/rg-create1.png)

![](/images/rg-create2.png)

![](/images/rg-create3.png)

![](/images/rg-create4.png)

![](/images/rg-create5.png)

### Erstellung der eigentlichen VM

![](/images/vm-create1.png)

![](/images/vm-create-image-selection.png)

![](/images/vm-create-vm-details.png)

![](/images/vm-create-image-and-size.png)

![](/images/vm-create-password.png)

![](/images/vm-create-hdd.png)

![](/images/vm-create-network.png)

![](/images/vm-create-nodiagnosis.png)

![](/images/vm-create-autoshutdown.png)

![](/images/vm-create-check-and-create.png)

## Erstellung mittels Azure CLI

```shell
az login -u username@edu.htl-villach.at

# Erzeugen einer Ressource Gruppe
az group create --location westeurope -n RG-Demo

# Erzeugen der virtuellen Maschine
az vm create -n vm-demo \
             -g RG-Demo \
             --size Standard_B1s \
             --image Canonical:0001-com-ubuntu-server-focal:20_04-lts:latest \
             --storage-sku Standard_LRS \
             --admin-user demo \
             --admin-password Changeme1234 \
             --public-ip-sku Basic \
             --public-ip-address-allocation static \
             --nsg ""

# "sku": "20_04-lts",

# Aktivieren des Auto-Shutdown
az vm auto-shutdown -n vm-demo \
                    -g RG-Demo \
                    --time 1600 \
                    --email "your.mail@edu.htl-villach.at" 
```

